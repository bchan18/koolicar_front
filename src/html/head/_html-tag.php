<!DOCTYPE html>
    <!--[if IE 9]>
        <html class="ie ie9 lte10 lte11">
    <![endif]-->

    <?php if (stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE 10')): ?>
        <html class="ie ie10 lte11">
    <?php else: ?>
        <!--[if !IE]><!-->
            <html>
        <!--<![endif]-->
    <?php endif; ?>