<?php include "head/_html-tag.php"; ?>

<head>
    <?php include "head/_meta-tag.html"; ?>

    <?php include "head/_assets.html"; ?>
</head>
<body>
    <?php include 'parts/modules/header.php' ?>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Locations</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <?php include 'parts/modules/account-infos.php' ?>
            </div>
            <div class="col-12 col-md-6">
                <?php include 'parts/modules/date-filters.php' ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <?php include 'parts/modules/renting-table.php' ?>
            </div>
        </div>
    </div>

    <?php include 'parts/modules/footer.php' ?>
</body>
</html>
