<div class="renting-table">
    <div class="renting-table__head">
        <div class="renting-table__head__label">Véhicule</div>
        <div class="renting-table__head__label">Début</div>
        <div class="renting-table__head__label">Fin</div>
        <div class="renting-table__head__label">Distance</div>
        <div class="renting-table__head__label">Durée</div>
        <div class="renting-table__head__label">Statut</div>
        <div class="renting-table__head__label">Montant</div>
        <div class="renting-table__head__label">Reçu</div>
    </div>
    <div class="renting-table__row">
        <div class="renting-table__data">
            <div class="title title--sans-bold title--smaller mg-bottom-5">
                Honda civic
            </div>
        </div>
        <div class="renting-table__data">
            <span class="renting-table__data__label">Début : </span>
            <span>22 Sept. <br class="hidden-xs"/>10h45</span>
        </div>
        <div class="renting-table__data">
            <span class="renting-table__data__label">Fin : </span>
            <span>23 Sept. <br class="hidden-xs"/>12h45</span>
        </div>
        <div class="renting-table__data">
            <span class="renting-table__data__label">Distance : </span>
            <span>145km</span>
        </div>
        <div class="renting-table__data">
            <span class="renting-table__data__label">Durée : </span>
            <span>15h34</span>
        </div>
        <div class="renting-table__data">
            <span class="renting-table__data__label">Statut : </span>
            <span>Terminé</span>
            <span class="hidden-xs text--green">Réduction  </span>
            <span class="hidden-xs text--red">Total </span>
        </div>
        <div class="renting-table__data">
            <div>
                <span class="renting-table__data__label text--green">Réduction : </span><span class="text text--sans-bold">20€</span>
            </div>
            <div>
                <span class="renting-table__data__label text--red">Total : </span><span class="text text--sans-bold">104,50€</span>
            </div>
            <div>
                <span class="renting-table__data__label">Montant : </span><span class="text text--sans-bold">145,50</span>
            </div>
        </div>
        <div class="renting-table__data">
            <a href="#" class="link">Télécharger <span class="hidden-md">un reçu</span></a>
        </div>
    </div>
    <div class="renting-table__row">
        <div class="renting-table__data">
            <div class="title title--sans-bold title--smaller mg-bottom-5">
                Honda civic
            </div>
        </div>
        <div class="renting-table__data">
            <span class="renting-table__data__label">Début : </span>
            <span>22 Sept. <br class="hidden-xs"/>10h45</span>
        </div>
        <div class="renting-table__data">
            <span class="renting-table__data__label">Fin : </span>
            <span>23 Sept. <br class="hidden-xs"/>12h45</span>
        </div>
        <div class="renting-table__data">
            <span class="renting-table__data__label">Distance : </span>
            <span>145km</span>
        </div>
        <div class="renting-table__data">
            <span class="renting-table__data__label">Durée : </span>
            <span>15h34</span>
        </div>
        <div class="renting-table__data">
            <span class="renting-table__data__label">Statut : </span>
            <span>Terminé</span>
        </div>
        <div class="renting-table__data">
            <div>
                <span class="renting-table__data__label">Montant : </span><span class="text text--sans-bold">145,50</span>
            </div>
        </div>
        <div class="renting-table__data">
            <a href="#" class="link">Télécharger <span class="hidden-md">un reçu</span></a>
        </div>
    </div>
    <div class="renting-table__row">
        <div class="renting-table__data">
            <div class="title title--sans-bold title--smaller mg-bottom-5">
                Honda civic
            </div>
        </div>
        <div class="renting-table__data">
            <span class="renting-table__data__label">Début : </span>
            <span>22 Sept. <br class="hidden-xs"/>10h45</span>
        </div>
        <div class="renting-table__data">
            <span class="renting-table__data__label">Fin : </span>
            <span>23 Sept. <br class="hidden-xs"/>12h45</span>
        </div>
        <div class="renting-table__data">
            <span class="renting-table__data__label">Distance : </span>
            <span>145km</span>
        </div>
        <div class="renting-table__data">
            <span class="renting-table__data__label">Durée : </span>
            <span>15h34</span>
        </div>
        <div class="renting-table__data">
            <span class="renting-table__data__label">Statut : </span>
            <span>Terminé</span>
        </div>
        <div class="renting-table__data">
            <div>
                <span class="renting-table__data__label">Montant : </span> <span class="text text--sans-bold">145,50</span>
            </div>
        </div>
        <div class="renting-table__data">
            <a href="#" class="link">Télécharger <span class="hidden-md">un reçu</span></a>
        </div>
    </div>
</div>
