<header class="header">
  <div class="container">
      <div class="row">
          <div class="col-12">
              <div class="header__container">
                  <a class="brand" href="#">
                      <span class="logo">
                          <img src="https://assets.koolicar.com/assets/header/logo@2x-49febdc89b7d8adcf5f779543fd762ee.png"/>
                      </span>
                  </a>
                  <nav class="header__menu">
                    <ul>
                        <li><a href="#">Inscription</a></li>
                        <li><a href="#">Connexion</a></li>
                        <li><a href="#">Aide</a></li>
                    </ul>
                    <a href="#" class="btn btn--green">Inscrire mon véhicule</a>
                  </nav>
                  <button class="header__btn-mobile js-btnMobile">
                      <span class="top"></span>
                      <span class="middle"></span>
                      <span class="third"></span>
                  </button>
              </div>
          </div>
      </div>
  </div>
</header>
