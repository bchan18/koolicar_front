jQuery(document).ready(function($){
    var dateStart = new Pikaday({
        field: $('.js-date-picker-start')[0],
        firstDay: 1,
        minDate: new Date(),
        onSelect: function() {
            console.log(this.getMoment().format('Do MMMM YYYY'));
        }
    });

    var dateEnd = new Pikaday({
        field: $('.js-date-picker-end')[0],
        firstDay: 1,
        minDate: new Date(),
        onSelect: function() {
            if($('.js-date-picker-start').val().length === 0) {
                dateStart.setDate(new Date());
            }
        }
     });

});
