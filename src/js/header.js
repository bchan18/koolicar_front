jQuery(document).ready(function($){
    var $btn = $('.js-btnMobile'),
        $open = false;

    $btn.on('click', function(){
        if($open === false) {
            $('.header').addClass('js-isOpen');
            $open = true;
        } else {
            $('.header').removeClass('js-isOpen');
            $open = false;
        }
    });
});
